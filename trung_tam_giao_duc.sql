-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th8 08, 2021 lúc 03:38 PM
-- Phiên bản máy phục vụ: 10.4.19-MariaDB
-- Phiên bản PHP: 8.0.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `trung_tam_giao_duc`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `dang_ky`
--

CREATE TABLE `dang_ky` (
  `id` int(11) NOT NULL,
  `ho_ten` varchar(100) COLLATE utf8mb4_bin NOT NULL,
  `so_dien_thoai` varchar(10) COLLATE utf8mb4_bin NOT NULL,
  `email` varchar(150) COLLATE utf8mb4_bin NOT NULL,
  `ngay_dang_ky` date NOT NULL,
  `gia_tien` int(11) NOT NULL,
  `id_lop` int(11) NOT NULL,
  `trang_thai` tinyint(4) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Đang đổ dữ liệu cho bảng `dang_ky`
--

INSERT INTO `dang_ky` (`id`, `ho_ten`, `so_dien_thoai`, `email`, `ngay_dang_ky`, `gia_tien`, `id_lop`, `trang_thai`) VALUES
(1, 'trung', '0912345678', 'trung@gmail.com', '2021-08-08', 5000000, 1, 1),
(2, 'hạnh', '0879339888', 'hanh@gmail.com', '2021-08-08', 2000000, 1, 1),
(3, 'bảo', '0352461090', 'bao@gmail.com', '2021-08-08', 1000000, 1, 1),
(9, 'Bảo', '0987654321', 'nguyenthanhtrung201002@gmail.com', '2021-08-08', 5000000, 1, 1),
(10, 'Bảo', '9088676765', 'nguyenthanhtrung201002@gmail.com', '2021-08-08', 5000000, 1, 1),
(11, 'nam', '09324324', 'nguyenthanhtrung201002@gmail.com', '2021-08-08', 5000000, 2, 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `danh_muc_khoa_hoc`
--

CREATE TABLE `danh_muc_khoa_hoc` (
  `id` int(11) NOT NULL,
  `ten_danh_muc` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `trang_thai` tinyint(4) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Đang đổ dữ liệu cho bảng `danh_muc_khoa_hoc`
--

INSERT INTO `danh_muc_khoa_hoc` (`id`, `ten_danh_muc`, `trang_thai`) VALUES
(1, 'Thiết kế và lập trình web', 1),
(2, 'Thiết kế và lập trình game', 1),
(3, 'Lập trình ứng dụng di dộng', 1),
(4, 'Thiết kế đồ họa cơ bản', 1),
(13, 'LẬP TRÌNH WEB NÂNG CAO VỚI LARAVEL FRAMEWORK', 1),
(14, 'LẬP TRÌNH WEB NÂNG CAO VỚI NODEJS ', 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `danh_muc_tin_tuc`
--

CREATE TABLE `danh_muc_tin_tuc` (
  `id` int(11) NOT NULL,
  `ten_danh_muc` varchar(200) COLLATE utf8mb4_bin NOT NULL,
  `trang_thai` tinyint(2) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Đang đổ dữ liệu cho bảng `danh_muc_tin_tuc`
--

INSERT INTO `danh_muc_tin_tuc` (`id`, `ten_danh_muc`, `trang_thai`) VALUES
(1, 'COVID_19', 1),
(2, 'Kế hoạch đào tạo', 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `giang_vien`
--

CREATE TABLE `giang_vien` (
  `id` int(11) NOT NULL,
  `ten_giang_vien` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `hinh_anh` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `thong_tin_ca_nhan` text COLLATE utf8mb4_bin NOT NULL,
  `trang_thai` tinyint(4) NOT NULL DEFAULT 1,
  `phan_quyen` tinyint(4) NOT NULL DEFAULT 2
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Đang đổ dữ liệu cho bảng `giang_vien`
--

INSERT INTO `giang_vien` (`id`, `ten_giang_vien`, `hinh_anh`, `thong_tin_ca_nhan`, `trang_thai`, `phan_quyen`) VALUES
(1, 'Hoàng Quang Thắng', 'tempsnip.png', 'Ngày sinh: 12/08/1996\r\nQuê quán: Hải Phòng  \r\nSố điên thoại: 0879339888\r\nGmail: thang@gmail.com', 1, 2),
(4, 'nam1', 'tempsnip.png', 'ngày sinh 1232', 1, 2);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `khoa_hoc`
--

CREATE TABLE `khoa_hoc` (
  `id` int(11) NOT NULL,
  `ten_khoa_hoc` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `thoi_gian` int(11) NOT NULL,
  `thong_tin` text COLLATE utf8mb4_bin NOT NULL,
  `ke_hoach_dao_tao` text COLLATE utf8mb4_bin NOT NULL,
  `hinh_anh` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `hoc_phi` int(11) NOT NULL,
  `trang_thai` tinyint(4) NOT NULL DEFAULT 1,
  `id_danh_muc_khoa_hoc` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Đang đổ dữ liệu cho bảng `khoa_hoc`
--

INSERT INTO `khoa_hoc` (`id`, `ten_khoa_hoc`, `thoi_gian`, `thong_tin`, `ke_hoach_dao_tao`, `hinh_anh`, `hoc_phi`, `trang_thai`, `id_danh_muc_khoa_hoc`) VALUES
(1, 'Lập trình PHP cơ bản', 6, 'Thiết kế và lâp trình website PHP chuyên nghiệp. Đào tạo theo nhu cầu doanh nghiêp ', 'Thiết kế wesite với HTML,CSS,JavaScript,PHP cơ bản, PHP nâng cao, Project cuối khóa', 'khoa_hoc_01.jpg', 5000000, 1, 1),
(2, 'Lập trình Java ', 6, 'Lập trình website với Java phù hợp với nhu cầu tuyển dụng của nhiều công ty lớn cũng như yêu cầu của thị trường tuyển dụng lập trình viên Java.\r\n      ', 'Khóa học cung cấp đầy đủ kiến thức cho Học viên về lập trình giao diện web (frontend): HTML, CSS, JavaScript, Jquery, Bootstrap.\r\nKhóa học cung cấp đầy đủ kiến thức lập trình backend bao gồm: Kỹ thuật lập trình Java cơ bản và lập trình hướng đối tượng. Làm việc với hệ quản trị CSDL MySQL. Hiểu và phát triển website trên kiến trúc mô hình MVC với JSP/Servlet. Các kiến thức liên quan tới tối ưu mã nguồn, đảm bảo tính bảo mật trong website. Hiểu các mô hình phát triển phầm mềm\r\n\r\n\r\n', 'khoa_hoc_06.jpg', 5000000, 1, 1),
(3, 'Lập trình ứng dụng di dộng ANDROID', 6, 'Khóa học tập trung đào tạo kỹ năng lập trình cho học viên, giúp học viên hiểu sâu bài học. Các bài labguide chi tiết giúp học viên dễ dàng tổng hợp kiến thức trên lớp. Môi trường chuyên nghiệp, trao đổi thân thiện, cởi mở với Giảng viên. Thời gian học linh động, phù hợp cho giảng viên và học viên.\r\n', 'Cung cấp cho học viên kỹ năng lập trình Java và lập trình ứng dụng trên nền tảng Android. Cung cấp kỹ năng phân tích, thiết kế, xây dựng ứng dụng Android - Hiểu và nắm bắt cách xây dựng dịch vụ web để xây dựng và quản lý ứng dụng - Giúp học viên biết đưa ứng dụng lên CH Play, kiếm tiền từ ứng dụng - Chia sẻ những kinh nghiệm trong thiết kế và lập trình ứng dụng di động từ Giảng viên.\r\n', 'khoa_hoc_03.jpg', 5000000, 1, 2);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `khuyen_mai`
--

CREATE TABLE `khuyen_mai` (
  `id` int(11) NOT NULL,
  `ma_khuyen_mai` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `ten_khuyen_mai` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `phan_tram_giam_gia` int(11) NOT NULL,
  `ngay_bat_dau` date NOT NULL,
  `ngay_ket_thuc` date NOT NULL,
  `trang_thai` tinyint(4) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Đang đổ dữ liệu cho bảng `khuyen_mai`
--

INSERT INTO `khuyen_mai` (`id`, `ma_khuyen_mai`, `ten_khuyen_mai`, `phan_tram_giam_gia`, `ngay_bat_dau`, `ngay_ket_thuc`, `trang_thai`) VALUES
(1, 'CHAOTANSINHVIEN', 'Chương trình chào tân sinh viên', 50, '2021-08-01', '2021-10-03', 1),
(2, 'CHAOHE', 'Chào hè năm 2021', 30, '2021-06-03', '2021-09-01', 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `lien_he`
--

CREATE TABLE `lien_he` (
  `id` int(11) NOT NULL,
  `noi_dung` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `phone` varchar(10) COLLATE utf8mb4_bin NOT NULL,
  `dia_chi` varchar(200) COLLATE utf8mb4_bin NOT NULL,
  `trang_thai` tinyint(4) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Đang đổ dữ liệu cho bảng `lien_he`
--

INSERT INTO `lien_he` (`id`, `noi_dung`, `email`, `phone`, `dia_chi`, `trang_thai`) VALUES
(1, 'Liên hệ admin', 'nguyenthanhtrung201002@gmail.com', '0898555917', 'Nam Từ Liêm-Hà Nội', 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `lop_hoc`
--

CREATE TABLE `lop_hoc` (
  `id` int(11) NOT NULL,
  `ten_lop_hoc` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `ca_hoc` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `thoi_gian_bat_dau` date NOT NULL,
  `dia_diem_hoc` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `so_cho` int(11) NOT NULL,
  `trang_thai` tinyint(4) NOT NULL DEFAULT 1,
  `id_khoa_hoc` int(11) NOT NULL,
  `id_giang_vien` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Đang đổ dữ liệu cho bảng `lop_hoc`
--

INSERT INTO `lop_hoc` (`id`, `ten_lop_hoc`, `ca_hoc`, `thoi_gian_bat_dau`, `dia_diem_hoc`, `so_cho`, `trang_thai`, `id_khoa_hoc`, `id_giang_vien`) VALUES
(1, 'PHP01', 'thứ 2 - thứ 6', '2022-03-26', 'Hà Nội', 25, 1, 1, 1),
(2, 'Java02', 'thứ 2 - thứ 6', '2022-03-26', 'Hồ Chì Minh', 29, 1, 2, 1),
(3, 'PHP02', 'thứ 2 - thứ 4', '2022-03-26', 'Hà Nội', 30, 1, 1, 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `quan_tri`
--

CREATE TABLE `quan_tri` (
  `id` int(11) NOT NULL,
  `ten_quan_tri` varchar(150) COLLATE utf8mb4_bin NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `quyen_han` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Đang đổ dữ liệu cho bảng `quan_tri`
--

INSERT INTO `quan_tri` (`id`, `ten_quan_tri`, `email`, `password`, `quyen_han`) VALUES
(1, 'Quản trị viên', 'admin@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tieu_de`
--

CREATE TABLE `tieu_de` (
  `id` int(11) NOT NULL,
  `ten_tieu_de` varchar(150) COLLATE utf8mb4_bin NOT NULL,
  `hinh_anh` varchar(200) COLLATE utf8mb4_bin NOT NULL,
  `trang_thai` tinyint(2) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Đang đổ dữ liệu cho bảng `tieu_de`
--

INSERT INTO `tieu_de` (`id`, `ten_tieu_de`, `hinh_anh`, `trang_thai`) VALUES
(1, 'Giao diện chính', 'background 02.jpg', 1),
(5, 'aa', 'The new Luna wolf logo!.jpg', 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tin_tuc`
--

CREATE TABLE `tin_tuc` (
  `id` int(11) NOT NULL,
  `ten_tin_tuc` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `hinh_anh` varchar(200) COLLATE utf8mb4_bin NOT NULL,
  `noi_dung` text COLLATE utf8mb4_bin NOT NULL,
  `ngay_tao` date NOT NULL,
  `trang_thai` tinyint(4) NOT NULL DEFAULT 1,
  `id_danh_muc_tin_tuc` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Đang đổ dữ liệu cho bảng `tin_tuc`
--

INSERT INTO `tin_tuc` (`id`, `ten_tin_tuc`, `hinh_anh`, `noi_dung`, `ngay_tao`, `trang_thai`, `id_danh_muc_tin_tuc`) VALUES
(1, 'Tình hình dịch tại khu vực Hà Nội', 'dich_benh.jpg', 'Sở Y tế Hà Nội nhận định, tình hình dịch bệnh trên địa bàn thành phố vẫn trong tầm kiểm soát. Tuy nhiên, nguy cơ đang ở mức rất cao và khó lường.', '2021-08-04', 1, 1);

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `dang_ky`
--
ALTER TABLE `dang_ky`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `danh_muc_khoa_hoc`
--
ALTER TABLE `danh_muc_khoa_hoc`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `danh_muc_tin_tuc`
--
ALTER TABLE `danh_muc_tin_tuc`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `giang_vien`
--
ALTER TABLE `giang_vien`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `khoa_hoc`
--
ALTER TABLE `khoa_hoc`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `khuyen_mai`
--
ALTER TABLE `khuyen_mai`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `lien_he`
--
ALTER TABLE `lien_he`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `lop_hoc`
--
ALTER TABLE `lop_hoc`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `quan_tri`
--
ALTER TABLE `quan_tri`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tieu_de`
--
ALTER TABLE `tieu_de`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tin_tuc`
--
ALTER TABLE `tin_tuc`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `dang_ky`
--
ALTER TABLE `dang_ky`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT cho bảng `danh_muc_khoa_hoc`
--
ALTER TABLE `danh_muc_khoa_hoc`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT cho bảng `danh_muc_tin_tuc`
--
ALTER TABLE `danh_muc_tin_tuc`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT cho bảng `giang_vien`
--
ALTER TABLE `giang_vien`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT cho bảng `khoa_hoc`
--
ALTER TABLE `khoa_hoc`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT cho bảng `khuyen_mai`
--
ALTER TABLE `khuyen_mai`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `lien_he`
--
ALTER TABLE `lien_he`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT cho bảng `lop_hoc`
--
ALTER TABLE `lop_hoc`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT cho bảng `quan_tri`
--
ALTER TABLE `quan_tri`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT cho bảng `tieu_de`
--
ALTER TABLE `tieu_de`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT cho bảng `tin_tuc`
--
ALTER TABLE `tin_tuc`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Các ràng buộc cho các bảng đã đổ
--

--
-- Các ràng buộc cho bảng `khoa_hoc`
--
ALTER TABLE `khoa_hoc`
  ADD CONSTRAINT `khoa_hoc_ibfk_1` FOREIGN KEY (`id`) REFERENCES `danh_muc_khoa_hoc` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
