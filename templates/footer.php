<a href="#" class="scroll_to_top icon-up-2" title="Scroll to top"></a>
<div class="custom_html_section"></div>

<script type="text/javascript" src="public/layout/js/jquery/jquery.min.js"></script>
<script type="text/javascript" src="public/layout/js/jquery/jquery-migrate.min.js"></script>
<script type="text/javascript" src="public/layout/js/jquery/ui/core.min.js"></script>
<script type="text/javascript" src="public/layout/js/jquery/ui/widget.min.js"></script>
<script type="text/javascript" src="public/layout/js/jquery/ui/tabs.min.js"></script>
<script type="text/javascript" src="public/layout/js/jquery/ui/accordion.min.js"></script>
<script type="text/javascript" src="public/layout/js/jquery/ui/effect.min.js"></script>
<script type="text/javascript" src="public/layout/js/jquery/ui/effect-fade.min.js"></script>
<script type="text/javascript" src="public/layout/js/jquery/jquery.blockUI.min.js"></script>
<script type="text/javascript" src="public/layout/js/jquery/jquery.cookie.min.js"></script>

<script type="text/javascript" src="public/layout/js/global.min.js"></script>
<script type="text/javascript" src="public/layout/js/core.utils.min.js"></script>
<script type="text/javascript" src="public/layout/js/core.init.min.js"></script>
<script type="text/javascript" src="public/layout/js/shortcodes/shortcodes.min.js"></script>

<script type="text/javascript" src="public/layout/js/rs-plugin/jquery.themepunch.tools.min.js"></script>
<script type="text/javascript" src="public/layout/js/rs-plugin/jquery.themepunch.revolution.min.js"></script>
<script type="text/javascript" src="public/layout/js/slider_init.js"></script>

<script type="text/javascript" src="public/layout/js/superfish.min.js"></script>
<script type="text/javascript" src="public/layout/js/jquery.slidemenu.min.js"></script>

<script type="text/javascript" src="public/layout/js/mediaelement/mediaelement-and-player.min.js"></script>
<script type="text/javascript" src="public/layout/js/mediaelement/wp-mediaelement.min.js"></script>

<script type="text/javascript" src="public/layout/js/core.messages/core.messages.min.js"></script>

<script type="text/javascript" src="public/layout/js/jquery.isotope.min.js"></script>
<script type="text/javascript" src="public/layout/js/hover/jquery.hoverdir.min.js"></script>
<script type="text/javascript" src="public/layout/js/prettyPhoto/jquery.prettyPhoto.min.js"></script>
<script type="text/javascript" src="public/layout/js/swiper/idangerous.swiper-2.7.min.js"></script>
<script type="text/javascript" src="public/layout/js/swiper/idangerous.swiper.scrollbar-2.4.min.js"></script>
<script type="text/javascript" src="public/layout/js/diagram/chart.min.js"></script>

<script type="text/javascript" src="public/layout/js/core.customizer/front.customizer.min.js"></script>
<script type="text/javascript" src="public/layout/js/skin.customizer.min.js"></script>



</body>


<!-- Mirrored from education-html.themerex.net/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 21 Jun 2018 19:27:13 GMT -->
</html>