<?php
require_once ("database.php");
class m_read_id extends database{
    public function read_user_khoa_hoc($id){
        $sql = "select * from khoa_hoc where id_danh_muc_khoa_hoc=?";
        $this->setQuery($sql);
        return $this->loadAllRows(array($id));
    }
    public function read_user_id_khoa_hoc($id){
        $sql = "select * from khoa_hoc where id=?";
        $this->setQuery($sql);
        return $this->loadRow(array($id));
    }
    public function read_user_id_tin_tuc($id){
        $sql = "select * from tin_tuc where id_danh_muc_tin_tuc=?";
        $this->setQuery($sql);
        return $this->loadAllRows(array($id));
    }
    public function read_user_id_chi_tiet_tin_tuc($id){
        $sql = "select tt.id,tt.ten_tin_tuc,tt.ngay_tao,tt.hinh_anh,tt.noi_dung,dm.ten_danh_muc 
                from tin_tuc as tt,danh_muc_tin_tuc as dm where tt.id_danh_muc_tin_tuc=dm.id and tt.id=?";
        $this->setQuery($sql);
        return $this->loadRow(array($id));
    }
    public function read_user_lop_hoc($id){
        $sql = "SELECT lp.id,lp.ten_lop_hoc,lp.ca_hoc,lp.thoi_gian_bat_dau,lp.dia_diem_hoc,lp.so_cho,gv.ten_giang_vien
                FROM lop_hoc AS lp, giang_vien as gv WHERE lp.id_giang_vien = gv.id AND lp.id_khoa_hoc=?";
        $this->setQuery($sql);
        return $this->loadAllRows(array($id));
    }
    public function read_id_danh_muc($id){
        $sql = "select * from danh_muc_khoa_hoc where id=?";
        $this->setQuery($sql);
        return $this->loadRow(array($id));
    }
    public function read_all_tieu_de(){
        $sql = "select * from tieu_de";
        $this->setQuery($sql);
        return $this->loadAllRows();
    }
    public function read_all_danh_muc_khoa_hoc(){
        $sql = "select * from danh_muc_khoa_hoc";
        $this->setQuery($sql);
        return $this->loadAllRows();
    }
    public function read_all_lien_he(){
        $sql = "select * from lien_he";
        $this->setQuery($sql);
        return $this->loadAllRows();
    }
    public function read_all_khuyen_mai(){
        $sql = "select * from khuyen_mai";
        $this->setQuery($sql);
        return $this->loadAllRows();
    }
    public function read_all_danh_nuc_tin_tuc(){
        $sql = "select * from danh_muc_tin_tuc";
        $this->setQuery($sql);
        return $this->loadAllRows();
    }
    public function read_all_danh_giang_vien(){
        $sql = "select * from giang_vien where trang_thai=1";
        $this->setQuery($sql);
        return $this->loadAllRows();
    }
    public function read_id_danh_nuc_tin_tuc($id){
        $sql = "select * from danh_muc_tin_tuc where id=?";
        $this->setQuery($sql);
        return $this->loadRow(array($id));
    }
    public function read_show_id_khuyen_mai($id){
        $sql = "select * from khuyen_mai where id=?";
        $this->setQuery($sql);
        return $this->loadRow(array($id));
    }
    public function read_id_show_dang_ky($id){
        $sql = "SELECT lp.ten_lop_hoc,lp.ca_hoc,lp.thoi_gian_bat_dau,lp.dia_diem_hoc,kh.hoc_phi,kh.ten_khoa_hoc,gv.ten_giang_vien 
                FROM lop_hoc AS lp, khoa_hoc AS kh , giang_vien AS gv WHERE lp.id_khoa_hoc= kh.id AND lp.id_giang_vien= gv.id and lp.id =?";
        $this->setQuery($sql);
        return $this->loadRow(array($id));
    }
}