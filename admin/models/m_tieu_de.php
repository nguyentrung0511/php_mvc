<?php
require_once ("database.php");
class m_tieu_de extends database{
    public function add_tieu_de($id,$ten_tieu_de,$hinh_anh,$trang_thai){
        $sql ="insert into tieu_de values(?,?,?,?)";
        $this->setQuery($sql);
        return $this->execute(array($id,$ten_tieu_de,$hinh_anh,$trang_thai));
    }
    public function edit_tieu_de($id,$ten_tieu_de,$hinh_anh,$trang_thai)
    {
        $sql="update tieu_de set ten_tieu_de=?,hinh_anh=?,trang_thai=? where id=?";
        $this->setQuery($sql);
        return $this->execute(array($ten_tieu_de,$hinh_anh,$trang_thai,$id));
    }
}