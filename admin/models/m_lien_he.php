<?php
require_once ("database.php");
class m_lien_he extends database{
    public function add_lien_he($id,$noi_dung,$email,$phone,$dia_chi,$trang_thai){
        $sql ="insert into lien_he values(?,?,?,?,?,?)";
        $this->setQuery($sql);
        return $this->execute(array($id,$noi_dung,$email,$phone,$dia_chi,$trang_thai));
    }
    public function edit_lien_he($noi_dung,$email,$phone,$dia_chi,$trang_thai,$id)
    {
        $sql="update lien_he set noi_dung=?,email=?,phone=?,dia_chi=?,trang_thai=? where id=?";
        $this->setQuery($sql);
        return $this->execute(array($noi_dung,$email,$phone,$dia_chi,$trang_thai,$id));
    }
    public function delete_lien_he($id){

        $sql = "delete from lien_he where id = ?";
        $this->setQuery($sql);
        return $this->execute(array($id));
    }
}
