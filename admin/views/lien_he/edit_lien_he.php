<?php
include ("nav.php");
include ("aside.php");
?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <!-- /.content-header -->
    <div class="card card-warning">
        <div class="card-header">
            <h3 class="card-title">Sửa Thông Tin Liên Hệ</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form method="post">
            <?php
            foreach ($show_dmkh as $lh){
            ?>
            <div class="card-body">
                <div class="form-group">
                    <label for="exampleInputProvince">Nôi Dung Liên Hệ</label>
                    <input type="text" class="form-control" name="noi_dung" value="<?php echo $lh->noi_dung;?>" autofocus="true" autocomplete="on" required>
                </div>
                <div class="form-group">
                    <label for="exampleInputProvince">Email</label>
                    <input type="email" class="form-control" name="email" value="<?php echo $lh->email;?>" autofocus="true" autocomplete="on" required>
                </div>
                <div class="form-group">
                    <label for="exampleInputProvince">Số Diện Thoại</label>
                    <input type="text" max="10" class="form-control" name="phone" value="<?php echo $lh->phone;?>" autofocus="true" autocomplete="on" required>
                </div>
                <div class="form-group">
                    <label for="exampleInputProvince">Địa Chỉ</label>
                    <input type="text" class="form-control" name="dia_chi" value="<?php echo $lh->dia_chi;?>" autofocus="true" autocomplete="on" required>
                </div>
                <div class="form-group">
                    <label for="exampleInputStatus">Trạng Thái</label>
                    <select class="form-control" name="trang_thai">
                        <option value="1" <?php if($lh->trang_thai ==1) echo"selected";?>>Hoạt Đông</option>
                        <option value="0" <?php  if($lh->trang_thai ==0) echo"selected";?>>Không Hoạt Đông</option>
                    </select>
                </div>
                <div>
                    <button class="btn btn-primary" name="btnSave">Submit</button>
                </div>
            </div>
            <?php
            }
            ?>
        </form>
    </div>
</div>