<?php
include ("nav.php");
include ("aside.php");
?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <!-- /.content-header -->
    <div class="card card-warning">
        <div class="card-header">
            <h3 class="card-title">Sửa Danh Mục Tin Tức</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form method="post">
            <?php
            foreach ( $show_id as $dmtt){
            ?>
            <div class="card-body">
                <div class="form-group">
                    <label for="exampleInputProvince">Tên Danh Mục</label>
                    <input type="text" class="form-control" name="ten_danh_muc" value="<?php echo $dmtt->ten_danh_muc;?>" autofocus="true" autocomplete="on" required>
                </div>

                <div class="form-group">
                    <label for="exampleInputStatus">Trạng Thái</label>
                    <select class="form-control" name="trang_thai" val>
                        <option value="1" <?php if($dmtt->trang_thai ==1) echo"selected";?>>Hoạt Đông</option>
                        <option value="0" <?php  if($dmtt->trang_thai ==0) echo"selected";?>>Không Hoạt Đông</option>
                    </select>
                </div>
                <div>
                    <button class="btn btn-primary" name="btnSave">Submit</button>
                </div>
            </div>
                <?php
            }
            ?>
        </form>
    </div>
</div>