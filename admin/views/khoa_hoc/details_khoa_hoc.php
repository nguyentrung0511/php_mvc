<!-- Main Sidebar Container -->
<?php
include ("nav.php");
include ("aside.php");
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1></h1>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <!-- Main content -->
                    <div class="invoice p-3 mb-3">
                        <!-- title row -->
                        <!-- Table row -->
                        <div class="row">
                            <div class="col-12 table-responsive">
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>Tên Khóa Học</th>
                                        <th>Thời Gian Học</th>
                                        <th>Học Phí</th>
                                        <th>Hình Ảnh</th>
                                        <th>Danh Mục Khóa Học</th>
                                        <th>Trạng Thái</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    foreach ( $show_details as $kh){

                                    ?>
                                    <tr>
                                        <td><?php echo $kh->ten_khoa_hoc ;?></td>
                                        <td><?php echo $kh->thoi_gian ;?> tháng</td>
                                        <td><?php echo number_format($kh->hoc_phi) ?> VND</td>
                                        <td><img src="../public/layout/imgkhoahoc/<?php echo  $kh->hinh_anh?>" style="width: 80px;"></td>
                                        <td><?php echo $kh->ten_danh_muc ;?></td>
                                        <td>
                                            <?php if($kh->trang_thai==1){?>
                                                <span class="badge badge-pill badge-info float">Hoạt Động</span>
                                            <?php } else if ($kh->trang_thai==0){?>
                                                <span class="badge badge-pill badge-warning float">Không Hoạt Động</span>
                                                <?php
                                            }
                                            ?>
                                        </td>
                                    </tr>
                                        <?php
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row invoice-info">
                            <div class="col-sm-4 invoice-col">
                                <strong> Thông Tin Khóa Học</strong><br>
                                <address style="width: 250px;">
                                    <?php
                                    foreach ( $show_details as $kh ) {
                                        echo $kh->thong_tin;
                                    }
                                    ?>
                                </address>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-4 invoice-col">
                                <strong> Kế Hoạch Đào Tạo</strong><br>

                                <address style="width: 250px;">
                                    <?php
                                    foreach ( $show_details as $kh ) {
                                        echo $kh->ke_hoach_dao_tao;
                                    }
                                    ?>
                                </address>
                            </div>
                            <!-- /.col -->

                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                        <!-- this row will not appear when printing -->

                        <div class="row no-print">
                            <div class="col-12">
                                <?php
                                foreach ( $show_details as $kh ) {
                                ?>
                                    <button type="button" class="btn btn-danger btn-lg float-right" onclick="window.location.href='delete_khoa_hoc.php?id=<?php echo $kh->id; ?>'" style="margin-right: 5px;">
                                        <i class="nav-icon fa fa-trash-alt"></i>
                                    </button>
                                <button type="button" class="btn btn-warning btn-lg float-right" onclick="window.location.href='edit_khoa_hoc.php?id=<?php echo $kh->id; ?>'" style="margin-right: 5px;"><i class="fa fa-edit"></i></button>

                                    <?php
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                    <!-- /.invoice -->
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->