<!-- Main Sidebar Container -->
<?php
include ("nav.php");
include ("aside.php");
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

    <!-- Main content -->
    <div class="content">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h1 class="page-title">
                </h1>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title m-b-0"><a href="add_khuyen_mai.php"><button type="button" class="btn btn-success btn-sm">Thêm khuyến mại</button></a></h3>
                        </div>
                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">Số thứ tự</th>
                                <th scope="col">Mã Khuyến Mãi</th>
                                <th scope="col">Tên Khuyến Mãi</th>
                                <th scope="col">Phần Trăm Khuyến Mại</th>
                                <th scope="col">Ngày Bắt Đầu</th>
                                <th scope="col">Ngày Kết Thúc</th>
                                <th scope="col">Trạng Thái</th>
                                <th scope="col">Hoạt động</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $i=1;
                            foreach ( $show_dmkh as $dmkh){

                                ?>
                                <tr>
                                    <th><?php echo $i++ ;?></th>
                                    <td><?php echo $dmkh->ma_khuyen_mai ;?></td>
                                    <td><?php echo $dmkh->ten_khuyen_mai ;?></td>
                                    <td><?php echo $dmkh->phan_tram_giam_gia;?>%</td>
                                    <td><?php echo date("d/m/Y",strtotime($dmkh->ngay_bat_dau))  ;?></td>
                                    <td><?php echo date("d/m/Y",strtotime($dmkh->ngay_ket_thuc)) ;?></td>
                                    <td>
                                        <?php if($dmkh->trang_thai==1){?>
                                            <span class="badge badge-pill badge-info float">Hoạt Động</span>
                                        <?php } else if ($dmkh->trang_thai==0){?>
                                            <span class="badge badge-pill badge-warning float">Không Hoạt Động</span>
                                            <?php
                                        }
                                        ?>
                                    </td>
                                    <td>
                                        <button type="button" class="btn btn-warning" onclick="window.location.href='edit_khuyen_mai.php?id=<?php echo $dmkh->id; ?>'"><i class="fa fa-edit"></i></button>
                                    </td>
                                </tr>
                                <?php
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
