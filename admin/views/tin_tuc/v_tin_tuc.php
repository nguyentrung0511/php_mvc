<!-- Main Sidebar Container -->
<?php
include ("nav.php");
include ("aside.php");
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

    <!-- Main content -->
    <div class="content">
        <div class="row">
            <?php
            if(isset($_GET['id_dmtt'])){

                ?>
                <div class="col-12">
                    <div class="callout callout-info">
                        <h5>Danh mục tin tức:
                            <?php
                            foreach ( $show_tendm as $dm) {
                                echo $dm->ten_danh_muc;
                            }
                            ?>
                        </h5>
                    </div>
                </div>
                <?php
            }
            ?>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-title m-b-0"><a href="add_tin_tuc.php"><button type="button" class="btn btn-success btn-sm">Thêm tin tức</button></a></h5>
                            <div class="card-tools">
                            </div>
                        </div>

                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">Số thứ tự</th>
                                <th scope="col">Tên tin tức</th>
                                <th scope="col">Ngày tạo</th>
                                <th scope="col">Hình ảnh</th>
                                <th scope="col">Nội dung</th>
                                <th scope="col">Trạng Thái</th>
                                <th scope="col">Hoạt động</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $i=1;
                            foreach ( $show_kh as $kh){

                            ?>
                            <tr>
                                <th><?php echo $i++ ;?></a></th>
                                <td ><?php echo $kh->ten_tin_tuc ;?></td>
                                <td><?php echo date("d/m/Y",strtotime($kh->ngay_tao)) ?></td>
                                <td><img src="../public/layout/imgtintuc/<?php echo  $kh->hinh_anh?>" style="width: 80px;"></td>
                                <td style="width: 400px;"><?php echo $kh->noi_dung ;?></td>
                                <td>
                                    <?php if($kh->trang_thai==1){?>
                                        <span class="badge badge-pill badge-info float">Hoạt Động</span>
                                    <?php } else if ($kh->trang_thai==0){?>
                                        <span class="badge badge-pill badge-warning float">Không Hoạt Động</span>
                                        <?php
                                    }
                                    ?>
                                </td>
                                <td><button type="button" class="btn btn-warning" onclick="window.location.href='edit_tin_tuc.php?id=<?php echo $kh->id; ?>'"><i class="fa fa-edit"></i></button>
                                    <button type="button" class="btn btn-danger" onclick="window.location.href='delete_tin_tuc.php?id=<?php echo $kh->id; ?>'"><i class="nav-icon fa fa-trash-alt"></i></button>
                                </td>
                                <td>
                            </tr>
                            </tbody>
                            <?php
                            }
                            ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
