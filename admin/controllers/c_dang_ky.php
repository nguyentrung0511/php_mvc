<?php
include("models/m_read_all.php");
include ("models/m_read_id.php");
include ("models/m_dang_ky.php");
include ("models/m_lop_hoc.php");
include ("SimpleImage.php");
class c_dang_ky{
    public function index(){
        $show= new m_read_all();
        $show_all= $show->read_all_show_dang_ky();
        $view = "views/dang_ky/v_dang_ky.php";
        include('templates/layout.php');
    }
    public function edit_dang_ky(){
        if (isset($_GET["id"])) {
            $id = $_GET["id"];
            $show = new m_read_all();
            $show_lop = $show->read_show_all_lop_hoc();
            $id_lp = new m_read_id();
            $lp= $id_lp->read_id_dang_ky($id);
            if (isset($_POST["btnSave"])) {
                $id_lop = $lp->id_lop;
                $trang_thai = 1;
                $dang_ky= new m_dang_ky();
                $edit_dk= $dang_ky->edit_dangky($id,$trang_thai);
                if ($edit_dk) {
                        $tl= new m_read_id();
                        $tra_lai = $tl->read_id_tra_lai_so_cho($id_lop);
                        $edit_sc= new m_lop_hoc();
                        $cap_nhat= $edit_sc->edit_so_cho( $id_lop,$tra_lai->so_cho-1);
                        echo "<script>alert('Sửa thông tin thành công');window.location='dang_ky.php'</script>";
                }
                echo "<script>alert('Sửa thông tin thành công');window.location='dang_ky.php'</script>";

            }

        }
        $view = "views/dang_ky/edit_dang_ky.php";
        include('templates/layout.php');
    }
}