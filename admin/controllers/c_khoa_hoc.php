<?php
include("models/m_read_all.php");
include ("models/m_danh_muc_khoa_hoc.php");
include ("models/m_khoa_hoc.php");
include ("models/m_read_id.php");
include ("SimpleImage.php");
class c_khoa_hoc{
    public function index(){
        if(isset($_GET['id_dmkh']))
        {
            $id_kh = $_GET['id_dmkh'];
            $show = new m_read_id();
            $show_kh=$show->read_id_khoa_hoc($id_kh);
            $show_tendm=$show->read_id_danh_muc_khoa_hoc($id_kh);
        }else{
            $show_all = new m_read_all();
            $show_kh = $show_all->read_all_khoa_hoc();
        }
        $view = "views/khoa_hoc/v_khoa_hoc.php";
        include('templates/layout.php');
    }
    public function details_khoahoc(){
            if(isset($_GET['id'])){
                $id = $_GET['id'];
                $show = new m_read_all();
                $show_details = $show->read_all_detail_khoa_hoc($id);
            }
        $view = "views/khoa_hoc/details_khoa_hoc.php";
        include('templates/layout.php');
    }
    public function add_khoahoc(){
        $show_dmkh= new m_read_all();
        $show_all= $show_dmkh->read_all_danh_muc_khoa_hoc();
        if(isset($_POST["btnSave"])) {
            $id = null;

            $ten_khoa_hoc = $_POST["ten_khoa_hoc"];
            $thoi_gian = $_POST["thoi_gian"];
            $thong_tin = $_POST["thong_tin"];
            $ke_hoach = $_POST["ke_hoach_dao_tao"];
            $hinh_anh = ($_FILES['f_hinh_anh']['error'] == 0) ? $_FILES['f_hinh_anh']['name'] : "";
            $hoc_phi = $_POST["hoc_phi"];
            $trang_thai = $_POST["trang_thai"];
            $id_danh_muc = $_POST["id_danh_muc"];
            $ma= $_POST["id_danh_muc"];
            $show = new m_read_all();
            $show_all_khoa_hoc = $show->read_show_all_khoa_hoc();
            foreach ($show_all_khoa_hoc as $kh) {
                if ($ten_khoa_hoc == $kh->ten_khoa_hoc) {
                    echo "<script>alert('Tên khóa học bị trùng thêm không thành công');window.location='add_khoa_hoc.php'</script>";
                    return;
                }
            }
            $add = new m_khoa_hoc();
            $add_khoa_hoc = $add->add_khoa_hoc($id,$ten_khoa_hoc,$thoi_gian,$thong_tin,$ke_hoach,$hinh_anh,$hoc_phi,$trang_thai,$id_danh_muc);
            if ($add_khoa_hoc) {
                if ($hinh_anh != "") {
                    move_uploaded_file($_FILES['f_hinh_anh']['tmp_name'], "../public/layout/imgkhoahoc/$hinh_anh");
                }
                echo "<script>window.location='khoa_hoc.php?id_dmkh=".$ma."'</script>";
            } else {
                echo "<script>alert('thêm không thành công')</script>";
            }
        }
        $view = "views/khoa_hoc/add_khoa_hoc.php";
        include('templates/layout.php');
    }
    public function edit_khoahoc(){
        if(isset($_GET["id"])){
            $id = $_GET["id"];
            $show = new m_read_id();
            $kh=$show->read_show_id_khoa_hoc($id);
            $show_dmkh= new m_read_all();
            $show_all= $show_dmkh->read_all_danh_muc_khoa_hoc();
            if (isset($_POST['btnSave'])){
                $ten_khoa_hoc = $_POST["ten_khoa_hoc"];
                $thoi_gian = $_POST["thoi_gian"];
                $thong_tin = $_POST["thong_tin"];
                $ke_hoach = $_POST["ke_hoach_dao_tao"];
                $hinh_anh = ($_FILES['f_hinh_anh']['error'] == 0) ? $_FILES['f_hinh_anh']['name'] :$kh->hinh_anh;
                $hoc_phi = $_POST["hoc_phi"];
                $trang_thai = $_POST["trang_thai"];
                $id_danh_muc = $_POST["id_danh_muc"];
                $ma=$_POST["id_danh_muc"];
//                var_dump($hinh_anh);
//                die();
                $edit = new m_khoa_hoc();
                $edit_it = $edit->edit_khoa_hoc($id,$ten_khoa_hoc,$thoi_gian,$thong_tin,$ke_hoach,$hinh_anh,$hoc_phi,$trang_thai,$id_danh_muc);
                if ($edit_it) {
                        if ($_FILES["f_hinh_anh"]["error"] == 0) {
                            move_uploaded_file($_FILES['f_hinh_anh']['tmp_name'], "../public/layout/imgkhoahoc/$hinh_anh");
                        }
                    echo "<script>alert('Cập nhật thành công');window.location='khoa_hoc.php?id_dmkh=".$ma."'</script>";
                } else {
                    echo "<script>alert('Cập nhật không thành công')</script>";
                }
            }
        }
        $view = 'views/khoa_hoc/edit_khoa_hoc.php';
        include("templates/layout.php");
    }
    public function delete_khoahoc(){

        if (isset($_GET["id"])) {
            $id=$_GET["id"];
            $tl= new m_read_id();
            $tra_lai = $tl->read_id_tra_lai($id);
            $show_ve = $tra_lai[0]->id;
            $show_all = new m_read_all();
            $count_class=$show_all->read_all_lop($id);
            if(count($count_class)>0){
                echo "<script>alert('Xóa không thành công ! Trong khóa học này đã tồn tại lớp học');window.location='khoa_hoc.php?id_dmkh=".$show_ve."'</script>";
            }else{
                $delete = new m_khoa_hoc();
                $kq = $delete->delete_khoa_hoc($id);
                echo "<script>alert('Xóa không thành công');window.location='khoa_hoc.php?id_dmkh=".$show_ve."'</script>";
            }
        }
    }
}