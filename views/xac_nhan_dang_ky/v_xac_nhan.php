<?php
include ("header.php");
?>
<div class="page_content_wrap">
    <div class="content_wrap">
        <div class="content">
            <article class="post_item post_item_404">
                <div class="post_content">
                    <h1 class="page_title">Chúc mừng bạn đã đăng ký thành công</h1>
                    <h2 class="page_subtitle">Chúc bạn có một khóa học thành công</h2>
                    <h3 class="page_subtitle">Vui lòng vào gmail xác nhận lại thông tin bạn đã đăng ký</h3>
                    <h4 class="page_description"><a href="index.php">Quay lại trang chủ</a></h4>
                </div>
            </article>
        </div>
    </div>
</div>
<?php
include ("footer.php");
?>