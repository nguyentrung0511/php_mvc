<!-- Testimonials footer -->
<footer class="testimonials_wrap sc_section bg_tint_dark post_ts_bg3">
    <div class="sc_section_overlay" data-bg_color="#1eaace" data-overlay="0">
        <div class="content_wrap">
            <!-- Testimonials section -->
            <div class="sc_testimonials sc_slider_swiper swiper-slider-container sc_slider_nopagination sc_slider_controls sc_slider_height_fixed aligncenter height_12em width_100per" data-old-height="12em" data-interval="7000">
                <div class="slides swiper-wrapper">
                    <?php
                    foreach ($kt as $km){
                    ?>
                    <div class="swiper-slide height_12em width_100per" data-style="width:100%;height:12em;">

                        <div class="sc_testimonial_item">
                            <div class="sc_testimonial_avatar">
                                <img alt="" src="public/layout/images/partners_03.jpg"></div>
                            <div class="sc_testimonial_content">
                                <p><?php echo $km->ten_khuyen_mai?></p>
                            </div>
                            <div class="sc_testimonial_author">
                                <a href="#">Khuyến mãi giảm đến: <?php echo $km->phan_tram_giam_gia?>%</a>
                            </div>
                        </div>
                    </div>
                        <?php
                    }
                    ?>
                </div>
                <div class="sc_slider_controls_wrap">
                    <a class="sc_slider_prev" href="#"></a>
                    <a class="sc_slider_next" href="#"></a>
                </div>
            </div>
            <!-- /Testimonials section -->
        </div>
    </div>
</footer>
<!-- /Testimonials footer -->
<!-- Contacts Footer -->
<footer class="contacts_wrap bg_tint_dark contacts_style_dark">
    <div class="content_wrap">
        <div class="logo">
            <a href="index-2.html">
                <img src="public/layout/images/logo_footer.png" alt="">
            </a>
        </div>
        <div class="contacts_address">
            <?php
            foreach ($lh as $lhe){
                ?>
                <address class="address_right">
                    Số điện thoai: <?php echo $lhe->phone?><br>
                    Gmail: <?php echo $lhe->email?>
                </address>
                <address class="address_left">
                    Địa chỉ: <?php echo $lhe->dia_chi?>
                </address>
                <?php
            }
            ?>
        </div>
        <div class="sc_socials sc_socials_size_big">
            <div class="sc_socials_item">
                <a href="#" target="_blank" class="social_icons social_facebook">
                    <span class="sc_socials_hover social_facebook"></span>
                </a>
            </div>
            <div class="sc_socials_item">
                <a href="#" target="_blank" class="social_icons social_pinterest">
                    <span class="sc_socials_hover social_pinterest"></span>
                </a>
            </div>
            <div class="sc_socials_item">
                <a href="#" target="_blank" class="social_icons social_twitter">
                    <span class="sc_socials_hover social_twitter"></span>
                </a>
            </div>
            <div class="sc_socials_item">
                <a href="#" target="_blank" class="social_icons social_gplus">
                    <span class="sc_socials_hover social_gplus"></span>
                </a>
            </div>
            <div class="sc_socials_item">
                <a href="#" target="_blank" class="social_icons social_rss">
                    <span class="sc_socials_hover social_rss"></span>
                </a>
            </div>
            <div class="sc_socials_item">
                <a href="#" target="_blank" class="social_icons social_dribbble">
                    <span class="sc_socials_hover social_dribbble"></span>
                </a>
            </div>
        </div>
    </div>
</footer>
<!-- /Contacts Footer -->